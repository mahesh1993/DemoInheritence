﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInheritence
{
    interface ITest
    {
        string GetName();
    }

    class Employee : ITest
    {
        public string GetName()
        {
            return "Mahesh";
        }
    }

    class Doctor : ITest
    {
        public string GetName()
        {
            return "Dinesh";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ITest My ;
            My = new Employee();
            Console.WriteLine(My.GetName());
            My = new Doctor();
            Console.WriteLine(My.GetName());
            Console.ReadLine();
        }
    }
}
